# -*- coding: utf-8 -*-

from django.contrib import admin, messages
from django.conf.urls import patterns, url
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.utils.translation import ugettext as _

from apps.morpheus.models import GraduateDeclaration, MorpheusProfile
from apps.merovingian.models import Course

from apps.morpheus.forms import SelectRespondentsForm

TEMPLATE_ROOT = 'admin/morpheus/graduatedeclaration/'


class GraduateDeclarationAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name', 'email', 'course_name', 'education_level', 'approximated_defense_date')

    def get_urls(self):
        urls = super(GraduateDeclarationAdmin, self).get_urls()
        urls = patterns('',
                        url(r'^select_respondents/$', self.admin_site.admin_view(self.select_respondents_view), name='morpheus_select_respondents')
                        ) + urls
        return urls

    def select_respondents_view(self, request):
        form = SelectRespondentsForm(request.POST or None)
        if request.method == 'POST':
            if form.is_valid():
                start_date = form.cleaned_data['start_date']
                end_date = form.cleaned_data['end_date']
                education_level = form.cleaned_data['education_level']

                csv_output = self.get_respondents(start_date, end_date, education_level)

                response = HttpResponse(csv_output.getvalue(), mimetype='text/csv')
                response['Content-Disposition'] = 'filename=data.csv'
                return response
        context = {}
        context['form'] = form
        return render_to_response(TEMPLATE_ROOT+'select_respondents.html', context, context_instance=RequestContext(request))

    def get_respondents(self, start_date, end_date, education_level):
        import csv
        import io

        declarations = GraduateDeclaration.objects.filter(approximated_defense_date__range=(start_date, end_date), education_level=education_level)

        output = io.BytesIO()
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)
        for declaration in declarations.all():
            row = []
            row.append(declaration.email)
            writer.writerow(row)
        return output


class MorpheusProfileAdmin(admin.ModelAdmin):
    list_display = ('user_profile', 'get_email', 'get_courses')
    search_fields = ('^user_profile__user__last_name', '^user_profile__user__username')
    ordering = ('user_profile__user__last_name',)
    list_per_page = 5

    filter_vertical = ('courses',)

    def get_email(self, obj):
        return obj.user_profile.user.email
    get_email.short_description = _(u'Email')

    def get_courses(self, obj):
        result = '<ul>'
        for m in obj.courses.all():
            result += '<li>%s</li>' % unicode(m)
        return result+'</ul>'
    get_courses.short_description = _(u'Courses')
    get_courses.allow_tags = True

    def formfield_for_manytomany(self, db_field, request, **kwargs):
        if db_field.name == 'courses':
            kwargs['queryset'] = Course.objects.didactic_offer_and_future()
        return super(MorpheusProfileAdmin, self).formfield_for_manytomany(db_field, request, **kwargs)


admin.site.register(GraduateDeclaration, GraduateDeclarationAdmin)
admin.site.register(MorpheusProfile, MorpheusProfileAdmin)

