from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('',
    url(r'^questionnaire/(?P<declaration_id>\d+)?$', 'apps.morpheus.views.questionnaire'),
    url(r'^learning_outcomes/(?P<declaration_id>\d+)?$', 'apps.morpheus.views.evaluate_learning_outcomes'),
    url(r'^print_declaration/(?P<declaration_id>\d+)?$', 'apps.morpheus.views.print_declaration'),
    url(r'^statistics/$', 'apps.morpheus.views.statistics_course_list'),
    url(r'^statistics/select/(?P<course_id>\d+)$', 'apps.morpheus.views.statistics_course_select'),
    url(r'^statistics/los/results/(?P<course_id>\d+)$', 'apps.morpheus.views.statistics_los_results'),
    url(r'^statistics/los/save/(?P<course_id>\d+)$', 'apps.morpheus.views.statistics_los_save'),
    url(r'^statistics/q/results/(?P<course_id>\d+)$', 'apps.morpheus.views.statistics_q_results'),
    url(r'^statistics/q/save/(?P<course_id>\d+)$', 'apps.morpheus.views.statistics_q_save'),
    url(r'^/?$', 'apps.morpheus.views.show_declaration'),
)

