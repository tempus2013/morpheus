# -*- coding: utf-8 -*-
from django.http import HttpResponse

from django.template import RequestContext
from django.shortcuts import render_to_response, redirect, get_object_or_404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.decorators import login_required
from apps.morpheus.config import *
from django.db.models import Max

from datetime import date

from qrcode import QRCode, ERROR_CORRECT_L
import StringIO
import hashlib

from apps.morpheus.forms import GraduateDeclarationForm, LearningOutcomesEvaluationForm, \
    Question1Form, Question2Form, Question3Form, Question4Form, Question5Form

from apps.morpheus.models import GraduateDeclaration, LearningOutcomesEvaluation, StudentAnswer
from apps.merovingian.models import Course
from apps.trinity.models import CourseLearningOutcome

from apps.whiterabbit.views import get_pdf_response

TEMPLATE_ROOT = 'morpheus/'


#---------------------------------------------------
#--- DEKLARACJA ABSOLWENTA
#---------------------------------------------------

def show_declaration(request):
    form = GraduateDeclarationForm(request.POST or None)
    if request.method == 'POST':
        if form.is_valid():
            try:  # Student drugi wypełnia deklarację. Pobierana jest jego poprzednia deklaracja i nadpisywane są dane.
                gd = GraduateDeclaration.objects.get(email=form.cleaned_data['email'])
            except GraduateDeclaration.DoesNotExist:  # Student wypełnia deklarację po raz pierwszy.
                gd = GraduateDeclaration()
            except GraduateDeclaration.MultipleObjectsReturned:  # W bazie są dwie deklaracje zarejestrowane na ten sam adres email. Takie sytuacjnie nie powinny mieć miejsca.
                gd = GraduateDeclaration.objects.filter(email=form.cleaned_data['email'])[0]

            gd.first_name = form.cleaned_data['first_name']
            gd.last_name = form.cleaned_data['last_name']
            gd.course_name = form.cleaned_data['course']
            gd.education_level = form.cleaned_data['education_level']
            gd.course_type = form.cleaned_data['course_type']
            gd.course_profile = form.cleaned_data['course_profile']
            gd.approximated_defense_date = form.cleaned_data['approximated_defense_date']
            gd.email = form.cleaned_data['email']
            gd.album_number = form.cleaned_data['album_number']
            gd.questionnaire = form.cleaned_data['questionaire']
            gd.save()

            return redirect(reverse('apps.morpheus.views.evaluate_learning_outcomes', kwargs={'declaration_id': gd.id}))
        else:
            messages.error(request, _(u'Please correct errors in your form'))
    kwargs = {'form': form}
    return render_to_response('morpheus/show_declaration.html', kwargs, context_instance=RequestContext(request))


#---------------------------------------------------
#--- OCENA EFEKTÓW KSZTAŁCENIA
#---------------------------------------------------

def evaluate_learning_outcomes(request, declaration_id):
    # Pobranie deklracji absolwenta.
    declaration = get_object_or_404(GraduateDeclaration, pk=declaration_id)

    # Pobranie kierunku. Jeżeli kierunek nie istnieje następuje przejście do ankiety.
    # Kierunek może nie istnieć ponieważ z formularza można wybrać wszystkie możliwe konfiguracje, a nie tylko konfiguracje, które rzeczywiście istnieją.
    try:
        course = Course.objects.filter(name=declaration.course_name, level=declaration.education_level, type=declaration.course_type, profile=declaration.course_profile).latest('start_date')
    except Course.DoesNotExist:
        return redirect(reverse('apps.morpheus.views.questionnaire', kwargs={'declaration_id': declaration.id}))

    # Pobranie kierunkowych efektów kształcenia dla kierunku.
    clos = CourseLearningOutcome.objects.filter(course=course)

    # Sprawdzenie czy kierunkowe efekty kształcenia dla wybranego kierunku zostały wprowadzone. Jeżeli nie następuje przejście do ankiety.
    if not clos.count():
        return redirect(reverse('apps.morpheus.views.questionnaire', kwargs={'declaration_id': declaration.id}))

    form = LearningOutcomesEvaluationForm(request.POST or None, learning_outcomes=clos)

    if request.method == 'POST':
        if form.is_valid():
            LearningOutcomesEvaluation.objects.filter(graduate_declaration=declaration).delete()
            for clo_id, evaluation in form.cleaned_data.iteritems():
                loe = LearningOutcomesEvaluation()
                loe.graduate_declaration = declaration
                loe.course = course
                loe.course_learning_outcome = CourseLearningOutcome.objects.get(pk=clo_id)
                loe.evaluation = evaluation
                loe.save()
            return redirect(reverse('apps.morpheus.views.questionnaire', kwargs={'declaration_id': declaration.id}))
        else:
            messages.error(request, _(u'Please correct errors in your form.'))

    template_context = {}
    template_context['declaration'] = declaration
    template_context['form'] = form
    return render_to_response('morpheus/evaluate_learning_outcomes.html', template_context, context_instance=RequestContext(request))


#---------------------------------------------------
#--- ANKIETA
#---------------------------------------------------

def questionnaire(request, declaration_id):
    declaration = get_object_or_404(GraduateDeclaration, pk=declaration_id)
    question_1_form = Question1Form(request.POST or None)
    question_2_form = Question2Form(request.POST or None, prefix='q2')
    question_3_form = Question3Form(request.POST or None, prefix='q3')
    question_4_form = Question4Form(request.POST or None, prefix='q4')
    question_5_form = Question5Form(request.POST or None, prefix='q5')

    if request.method == 'POST':
        if question_1_form.is_valid() and question_2_form.is_valid() and question_3_form.is_valid() and question_4_form.is_valid() and question_5_form.is_valid():
            question_1_subquestion_1, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_1_ID)
            question_1_subquestion_1.question_choice_answer = question_1_form.cleaned_data['subquestion_1']
            question_1_subquestion_1.save()

            question_1_subquestion_2, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_2_ID)
            question_1_subquestion_2.question_choice_answer = question_1_form.cleaned_data['subquestion_2']
            question_1_subquestion_2.save()

            question_1_subquestion_3, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_3_ID)
            question_1_subquestion_3.question_choice_answer = question_1_form.cleaned_data['subquestion_3']
            question_1_subquestion_3.save()

            question_1_subquestion_4, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_4_ID)
            question_1_subquestion_4.question_choice_answer = question_1_form.cleaned_data['subquestion_4']
            question_1_subquestion_4.save()

            question_1_subquestion_5, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_5_ID)
            question_1_subquestion_5.question_choice_answer = question_1_form.cleaned_data['subquestion_5']
            question_1_subquestion_5.save()

            question_1_subquestion_6, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_6_ID)
            question_1_subquestion_6.question_choice_answer = question_1_form.cleaned_data['subquestion_6']
            question_1_subquestion_6.save()

            question_1_subquestion_7, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_7_ID)
            question_1_subquestion_7.question_choice_answer = question_1_form.cleaned_data['subquestion_7']
            question_1_subquestion_7.save()

            question_1_subquestion_8, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_8_ID)
            question_1_subquestion_8.question_choice_answer = question_1_form.cleaned_data['subquestion_8']
            question_1_subquestion_8.save()

            question_1_subquestion_9, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_9_ID)
            question_1_subquestion_9.question_choice_answer = question_1_form.cleaned_data['subquestion_9']
            question_1_subquestion_9.save()

            question_1_subquestion_10, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_10_ID)
            question_1_subquestion_10.question_choice_answer = question_1_form.cleaned_data['subquestion_10']
            question_1_subquestion_10.save()

            question_2, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_2_ID)
            question_2.question_choice_answer = question_2_form.cleaned_data['answers']
            question_2.save()

            question_3, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_3_ID)
            question_3.question_choice_answer = question_3_form.cleaned_data['answers']
            question_3.save()

            question_4, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_4_ID)
            question_4.question_choice_answer = question_4_form.cleaned_data['answers']
            question_4.save()

            question_5, created = StudentAnswer.objects.get_or_create(declaration=declaration, question=QUESTION_5_ID)
            question_5.question_text_answer = question_5_form.cleaned_data['answer']
            question_5.save()

            return redirect(reverse('apps.morpheus.views.print_declaration', kwargs={'declaration_id': declaration.id}))
        else:
            messages.error(request, _(u'Please correct errors in your form.'))

    template_context = {}
    template_context['declaration'] = declaration
    template_context['question_1_form'] = question_1_form
    template_context['question_2_form'] = question_2_form
    template_context['question_3_form'] = question_3_form
    template_context['question_4_form'] = question_4_form
    template_context['question_5_form'] = question_5_form
    return render_to_response('morpheus/questionnaire.html', template_context, context_instance=RequestContext(request))


#---------------------------------------------------
#--- WYDRUK DEKARACJI ABSOLWENTA
#---------------------------------------------------

def print_declaration(request, declaration_id):
    declaration = get_object_or_404(GraduateDeclaration, pk=declaration_id)

    template_context = {}
    template_context['first_name'] = declaration.first_name
    template_context['last_name'] = declaration.last_name
    template_context['course'] = declaration.course_name
    template_context['education_level'] = declaration.education_level
    template_context['album_number'] = declaration.album_number
    template_context['qr_code'] = generate_qr_code(declaration.first_name, declaration.last_name, declaration.course_name, declaration.album_number)
    
    template_name = TEMPLATE_ROOT + 'print_declaration.html'
    file_name = 'deklaracja'
    
    return get_pdf_response(request, template_context, template_name, file_name, 'apps.morpheus.views.declaration')


def generate_qr_code(first_name, last_name, course, album_number):
    m = hashlib.md5()  # Creating MD5 object
    m.update("UMCS%s" % album_number)  # Generating MD5 hash

    qr = QRCode(version=None, error_correction=ERROR_CORRECT_L)
    qr.add_data("%s %s %s" % (first_name, last_name, m.hexdigest()[:5]))  # Adding QR code content
    qr.make(fit=True)  # Geneartin QR code
    im = qr.make_image()  # Creating a QR code image
    
    output = StringIO.StringIO()
    im.save(output)
    contents = output.getvalue().encode("base64")
    output.close()

    return 'data:image/png;base64, %s' % contents


#---------------------------------------------------
#--- STATISTICS
#---------------------------------------------------

from apps.morpheus.models import MorpheusProfile


@login_required
def statistics_course_list(request):
    if not MorpheusProfile.objects.filter(user_profile=request.user.get_profile()).exists() and not request.user.is_superuser:
        messages.error(request, 'Nie masz uprawnień do przeglądania tej strony')
        return redirect('/')

    courses = MorpheusProfile.objects.get_courses_names(request.user)
    context = {'courses': courses}
    return render_to_response('morpheus/statistics_course_list.html', context, context_instance=RequestContext(request))

@login_required
def statistics_course_select(request, course_id):
    if not MorpheusProfile.objects.filter(user_profile=request.user.get_profile()).exists() and not request.user.is_superuser:
        messages.error(request, 'Nie masz uprawnień do przeglądania tej strony')
        return redirect('/')

    course = get_object_or_404(Course, id=course_id)
    _courses = Course.objects.filter(name=course.name)

    # Stworzenie listy kierunków do wyboru.
    # Poniższa pętla pozostawia tylko po jednym kierunku ze wszystkich roczników
    # oraz po jednym kierunku pierwszego stopnia dla każdego typu ale bez względu na profil
    __courses = []
    for _course in _courses:
        item = {}
        item['id'] = _course.id
        item['name'] = _course.name
        item['level'] = _course.level
        item['type'] = _course.type
        item['profile'] = _course.profile

        add = True
        for __course in __courses:
            if __course['name'] == item['name'] and __course['level'] == item['level'] and __course['type'] == item['type'] and __course['profile'] == item['profile']:
                add = False
                break
            elif __course['name'] == item['name'] and __course['level'].id == item['level'].id == 8 and __course['type'] == item['type']:
                add = False
                break

        if add:
            __courses.append(item)

    # Dodanie informacji o liczbie ocen efektów i ankiet
    for __course in __courses:
        if __course['level'].id == 8:  # Studia 1 stopnia
            __course['los'] = LearningOutcomesEvaluation.objects.filter(
                course__name=__course['name'],
                course__level=__course['level'],
                course__type=__course['type']
            ).distinct('graduate_declaration').count()

            __course['qs'] = StudentAnswer.objects.filter(
                declaration__course_name=__course['name'],
                declaration__education_level=__course['level'],
                declaration__course_type=__course['type'],
            ).distinct('declaration').count()
        else:
            __course['los'] = LearningOutcomesEvaluation.objects.filter(
                course__name=__course['name'],
                course__level=__course['level'],
                course__type=__course['type'],
                course__profile=__course['profile']
            ).distinct('graduate_declaration').count()

            __course['qs'] = StudentAnswer.objects.filter(
                declaration__course_name=__course['name'],
                declaration__education_level=__course['level'],
                declaration__course_type=__course['type'],
                declaration__course_profile=__course['profile']
            ).distinct('declaration').count()

    context = {'courses': __courses}
    return render_to_response('morpheus/statistics_course_select.html', context, context_instance=RequestContext(request))


#---------------------------------------------------
#--- STATISTICS LOS
#---------------------------------------------------

from apps.trinity.models import EducationCategory
import csv
import io
from apps.syjon.lib.functions import utf2ascii


@login_required
def statistics_los_results(request, course_id):
    if not MorpheusProfile.objects.filter(user_profile=request.user.get_profile()).exists() and not request.user.is_superuser:
        messages.error(request, 'Nie masz uprawnień do przeglądania tej strony')
        return redirect('/')

    course = get_object_or_404(Course, id=course_id)

    knowledge = EducationCategory.objects.get(name_pl='wiedza')
    skills = EducationCategory.objects.get(name_pl='umiejętności')
    competences = EducationCategory.objects.get(name_pl='kompetencje społeczne')

    year = request.GET.get('y', None)

    results_knowledge = get_los_evaluation_for_category(course, knowledge, year)
    results_skills = get_los_evaluation_for_category(course, skills, year)
    results_competences = get_los_evaluation_for_category(course, competences, year)

    context = {
        'course': course,
        'results_knowledge': results_knowledge,
        'results_skills': results_skills,
        'results_competences': results_competences,
        'year': year
    }
    return render_to_response('morpheus/statistics_los_results.html', context, context_instance=RequestContext(request))

def get_los_evaluation_for_category(course, category, year=None):
    if course.level.id == 8:  # Studia 1 stopnia:
        _results = LearningOutcomesEvaluation.objects.filter(
            course__name=course.name,
            course__level=course.level,
            course__type=course.type,
            course_learning_outcome__education_category=category)
    else:
        _results = LearningOutcomesEvaluation.objects.filter(
            course__name=course.name,
            course__level=course.level,
            course__type=course.type,
            course__profile=course.profile,
            course_learning_outcome__education_category=category)

    if year:
        year = int(year)
        date_from = date(year, 10, 1)
        date_to = date(year + 1, 10, 1)
        _results = _results.filter(
            graduate_declaration__creation_date__gte=date_from,
            graduate_declaration__creation_date__lte=date_to
        )

    __results = []
    for _result in _results:
        # Item to słownik ze statystykami efektu kształcenia
        result_evaluation = _result.evaluation + 1  # Zmiana skali z 0-2 na 1-3
        item = {}
        item['lo'] = _result.course_learning_outcome  # Efekt kształcenia
        item['count'] = 1  # Liczba ocenionych efektów kształcenia
        item['sum'] = result_evaluation  # Suma ocen efektów kształcenia
        item['avg'] = 0  # Średnia ocen efektów kształcenia
        item[1] = 0  # Suma ocen 1 (słabo)
        item[2] = 0  # Suma ocen 2 (średnio)
        item[3] = 0  # Suma ocen 3 (dobrze)
        item[result_evaluation] += 1

        # Sprawdzenie czy słownik z wynikami efektu kształcenia znajduje się już na liście wyników
        add = True  # Założenie, że słownik item powinien zostać dodany do listy z wynikami
        for __result in __results:
            if __result['lo'].symbol == item['lo'].symbol:  # Słownik ze statystykami efektu kształcenia już znajduje się na liście z wynikami
                add = False
                __item = __result
                break

        if add:  # Słownik z wynikami efektu kształcenia jeszcze nie znajduje się na liście i nalezy go dodać
            __results.append(item)
        else:  # Słownik z wynikami efektu kształcenia już znajduje się na liście i należy go zaktualizować
            __item['count'] += 1
            __item['sum'] += item['sum']
            __item[result_evaluation] += 1

    for item in __results:
        item['avg'] = "%.2f%%" % (round(item['sum']/float(item['count']), 2)/3.0*100)
        item['avg_1'] = "%.2f%%" % (item[1]/float(item['count'])*100)
        item['avg_2'] = "%.2f%%" % (item[2]/float(item['count'])*100)
        item['avg_3'] = "%.2f%%" % (item[3]/float(item['count'])*100)
    return __results


@login_required
def statistics_los_save(request, course_id):
    # Pobranie danych do analizy
    course = get_object_or_404(Course, id=course_id)

    knowledge = EducationCategory.objects.get(name_pl='wiedza')
    skills = EducationCategory.objects.get(name_pl='umiejętności')
    competences = EducationCategory.objects.get(name_pl='kompetencje społeczne')

    results_knowledge = get_los_evaluation_for_category(course, knowledge)
    results_skills = get_los_evaluation_for_category(course, skills)
    results_competences = get_los_evaluation_for_category(course, competences)

    # Stworzenie pliku z danymi do analizy
    output = io.BytesIO()
    writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    # Tu stwórz plik csv
    writer.writerow(['symbol', 'opis', 'liczba ocen', 'średnia arytmetyczna ocen w skali 1-3', 'słabo (1)', 'przeciętnie (2)', 'dobrze (3)'])
    for results in [results_knowledge, results_skills, results_competences]:
        for result in results:
            row = []
            row.append(result['lo'].symbol)
            row.append(unicode(result['lo'].description).encode('utf-8'))
            row.append(result['count'])
            row.append(round(result['sum']/float(result['count']), 2))
            row.append("%.2f%%" % (round(result[1]/float(result['count']), 2)*100))
            row.append("%.2f%%" % (round(result[2]/float(result['count']), 2)*100))
            row.append("%.2f%%" % (round(result[3]/float(result['count']), 2)*100))
            writer.writerow(row)

    # Stworzenie nazwy pliku do analizy
    filename = ''
    filename += utf2ascii(unicode(course))
    filename += ' - ocena efektow ksztalcenia.csv'
    filename = filename.replace(',', ' ')  # Przecinki nie były akceptowan w nazwie pliku
    response = HttpResponse(output.getvalue(), mimetype='text/csv')
    response['Content-Disposition'] = 'filename="%s"' % filename
    return response

#---------------------------------------------------
#--- SŁOWNIK ZE STRUKTURĄ PYTAŃ
#---------------------------------------------------

from apps.morpheus.config import *


class QuestionsResults(object):

    def __init__(self):
        self.data = list()

        self.data.append({
            'question': unicode(QUESTION_1_CONTENT).encode('utf-8'),
            'answer_1_content': unicode(QUESTION_1_ANSWER_1_CONTENT).encode('utf-8'),
            'answer_2_content': unicode(QUESTION_1_ANSWER_2_CONTENT).encode('utf-8'),
            'answer_3_content': unicode(QUESTION_1_ANSWER_3_CONTENT).encode('utf-8'),
            'answer_4_content': unicode(QUESTION_1_ANSWER_4_CONTENT).encode('utf-8'),
            'subquestion_1_content': unicode(SUBQUESTION_1_CONTENT).encode('utf-8'),
            'subquestion_1_answers': [0, 0, 0, 0],
            'subquestion_2_content': unicode(SUBQUESTION_2_CONTENT).encode('utf-8'),
            'subquestion_2_answers': [0, 0, 0, 0],
            'subquestion_3_content': unicode(SUBQUESTION_3_CONTENT).encode('utf-8'),
            'subquestion_3_answers': [0, 0, 0, 0],
            'subquestion_4_content': unicode(SUBQUESTION_4_CONTENT).encode('utf-8'),
            'subquestion_4_answers': [0, 0, 0, 0],
            'subquestion_5_content': unicode(SUBQUESTION_5_CONTENT).encode('utf-8'),
            'subquestion_5_answers': [0, 0, 0, 0],
            'subquestion_6_content': unicode(SUBQUESTION_6_CONTENT).encode('utf-8'),
            'subquestion_6_answers': [0, 0, 0, 0],
            'subquestion_7_content': unicode(SUBQUESTION_7_CONTENT).encode('utf-8'),
            'subquestion_7_answers': [0, 0, 0, 0],
            'subquestion_8_content': unicode(SUBQUESTION_8_CONTENT).encode('utf-8'),
            'subquestion_8_answers': [0, 0, 0, 0],
            'subquestion_9_content': unicode(SUBQUESTION_9_CONTENT).encode('utf-8'),
            'subquestion_9_answers': [0, 0, 0, 0],
            'subquestion_10_content': unicode(SUBQUESTION_10_CONTENT).encode('utf-8'),
            'subquestion_10_answers': [0, 0, 0, 0],
            'answers': 0
        })

        self.data.append({
            'question': unicode(QUESTION_2_CONTENT).encode('utf-8'),
            'answer_1_content': unicode(QUESTION_2_ANSWER_1_CONTENT).encode('utf-8'),
            'answer_1_results': 0,
            'answer_2_content': unicode(QUESTION_2_ANSWER_2_CONTENT).encode('utf-8'),
            'answer_2_results': 0,
            'answers': 0
        })
        self.data.append({
            'question': unicode(QUESTION_3_CONTENT).encode('utf-8'),
            'answer_1_content': unicode(QUESTION_3_ANSWER_1_CONTENT).encode('utf-8'),
            'answer_1_results': 0,
            'answer_2_content': unicode(QUESTION_3_ANSWER_2_CONTENT).encode('utf-8'),
            'answer_2_results': 0,
            'answer_3_content': unicode(QUESTION_3_ANSWER_3_CONTENT).encode('utf-8'),
            'answer_3_results': 0,
            'answer_4_content': unicode(QUESTION_3_ANSWER_4_CONTENT).encode('utf-8'),
            'answer_4_results': 0,
            'answers': 0
        })
        self.data.append({
            'question': unicode(QUESTION_4_CONTENT).encode('utf-8'),
            'answer_1_content': unicode(QUESTION_4_ANSWER_1_CONTENT).encode('utf-8'),
            'answer_1_results': 0,
            'answer_2_content': unicode(QUESTION_4_ANSWER_2_CONTENT).encode('utf-8'),
            'answer_2_results': 0,
            'answer_3_content': unicode(QUESTION_4_ANSWER_3_CONTENT).encode('utf-8'),
            'answer_3_results': 0,
            'answer_4_content': unicode(QUESTION_4_ANSWER_4_CONTENT).encode('utf-8'),
            'answer_4_results': 0,
            'answer_5_content': unicode(QUESTION_4_ANSWER_5_CONTENT).encode('utf-8'),
            'answer_5_results': 0,
            'answers': 0
        })
        self.data.append({
            'question': unicode(QUESTION_5_CONTENT).encode('utf-8'),
            'answers': []
        })

    def fill(self, answers):
        for answer in answers:
            question_results = get_question_results(self, answer)
            question = answer.question
            if question == 1:
                add_to_question_1(question_results, answer)
            elif question == 2:
                add_to_question_2(question_results, answer)
            elif question == 3:
                add_to_question_3(question_results, answer)
            elif question == 4:
                add_to_question_4(question_results, answer)
            elif question == 5:
                add_to_question_5(question_results, answer)

        # W przypadku pierwszego pytania sumowane są wyszystkie odpowiedzi ze wszystkich podpytń więc sumę wszystkich odpowiedzi należy
        # podzielić przez liczbę odpowiedzi żeby otrzymać liczbę odpowiedzi jednego studenta do jednego pytania.
        self.data[0]['answers'] /= 10

#---------------------------------------------------
#--- STATISTICS Q
#---------------------------------------------------


def statistics_q_results(request, course_id):
    if not MorpheusProfile.objects.filter(user_profile=request.user.get_profile()).exists() and not request.user.is_superuser:
        messages.error(request, 'Nie masz uprawnień do przeglądania tej strony')
        return redirect('/')

    course = get_object_or_404(Course, id=course_id)
    year = request.GET.get('y', None)

    if course.level.id == 8:  # Studia 1 stopnia
        answers = StudentAnswer.objects.filter(
            declaration__course_name=course.name,
            declaration__education_level=course.level,
            declaration__course_type=course.type,
        )
    else:
        answers = StudentAnswer.objects.filter(
            declaration__course_name=course.name,
            declaration__education_level=course.level,
            declaration__course_type=course.type,
            declaration__course_profile=course.profile
        )

    if year:
        year = int(year)
        date_from = date(year, 10, 1)
        date_to = date(year + 1, 10, 1)
        answers = answers.filter(
            declaration__creation_date__gte=date_from,
            declaration__creation_date__lte=date_to
        )

    questions_results = QuestionsResults()
    questions_results.fill(answers)


    context = {
        'course': course,
        'results': questions_results,
        'year': year
    }
    return render_to_response('morpheus/statistics_q_results.html', context, context_instance=RequestContext(request))


def get_question_results(questions_results, answer):
    """
    Zwraca słownik pytania w zależności od odpowiedzi jaka jest dodawana. Przez słownik sytania rozumie się strukturę przechowującą
    treści pytań i uzyskane odpowiedzi.
    """
    question = answer.question
    if question == 1:
        return questions_results.data[0]
    elif question == 2:
        return questions_results.data[1]
    elif question == 3:
        return questions_results.data[2]
    elif question == 4:
        return questions_results.data[3]
    elif question == 5:
        return questions_results.data[4]


#---------------------------------------------------
#--- QUESTION 1
#---------------------------------------------------

def add_to_question_1(question_results, answer):
    """
    Dodaje odpowiedź do struktury pytania 1. Obie są przekazywane jako parametry.
    """
    question_results['answers'] += 1
    answer_value = (answer.question_choice_answer % 10) - 1
    if answer.subquestion == 1:
        question_results['subquestion_1_answers'][answer_value] += 1
    elif answer.subquestion == 2:
        question_results['subquestion_2_answers'][answer_value] += 1
    elif answer.subquestion == 3:
        question_results['subquestion_3_answers'][answer_value] += 1
    elif answer.subquestion == 4:
        question_results['subquestion_4_answers'][answer_value] += 1
    elif answer.subquestion == 5:
        question_results['subquestion_5_answers'][answer_value] += 1
    elif answer.subquestion == 6:
        question_results['subquestion_6_answers'][answer_value] += 1
    elif answer.subquestion == 7:
        question_results['subquestion_7_answers'][answer_value] += 1
    elif answer.subquestion == 8:
        question_results['subquestion_8_answers'][answer_value] += 1
    elif answer.subquestion == 9:
        question_results['subquestion_9_answers'][answer_value] += 1
    elif answer.subquestion == 10:
        question_results['subquestion_10_answers'][answer_value] += 1


def save_question_1(questions_results, writer):
    q = questions_results.data[0]['question']
    qa = questions_results.data[0]['answers']
    a1 = questions_results.data[0]['answer_1_content']
    a2 = questions_results.data[0]['answer_2_content']
    a3 = questions_results.data[0]['answer_3_content']
    a4 = questions_results.data[0]['answer_4_content']

    for i in range(1, 11):
        sq = questions_results.data[0]['subquestion_%s_content' % i]  # Treść podpytania
        sqa1 = questions_results.data[0]['subquestion_%s_answers' % i][0]  # Liczba odpowiedzi 1
        sqa2 = questions_results.data[0]['subquestion_%s_answers' % i][1]  # Liczba odpowiedzi 2
        sqa3 = questions_results.data[0]['subquestion_%s_answers' % i][2]  # Liczba odpowiedzi 3
        sqa4 = questions_results.data[0]['subquestion_%s_answers' % i][3]  # Liczba odpowiedzi 4
        x = sum(questions_results.data[0]['subquestion_%s_answers' % i])  # Liczba wszystkich odpowiedzi

        sqa1r = sqa1/float(x)*100 if x else 0  # Procentowy udział odpowiedzi 1 we wszystkich odpowiedziach
        sqa2r = sqa2/float(x)*100 if x else 0  # Procentowy udział odpowiedzi 2 we wszystkich odpowiedziach
        sqa3r = sqa3/float(x)*100 if x else 0  # Procentowy udział odpowiedzi 3 we wszystkich odpowiedziach
        sqa4r = sqa4/float(x)*100 if x else 0  # Procentowy udział odpowiedzi 4 we wszystkich odpowiedziach

        writer.writerow([q, sq, a1, sqa1, "%.2f%%" % sqa1r])
        writer.writerow([q, sq, a2, sqa2, "%.2f%%" % sqa2r])
        writer.writerow([q, sq, a3, sqa3, "%.2f%%" % sqa3r])
        writer.writerow([q, sq, a4, sqa4, "%.2f%%" % sqa4r])


#---------------------------------------------------
#--- QUESTION 2
#---------------------------------------------------

def add_to_question_2(question_results, answer):
    """
    Dodaje odpowiedź do struktury pytania 2. Obie są przekazywane jako parametry.
    """
    question_results['answers'] += 1
    if answer.question_choice_answer == 21:
        question_results['answer_1_results'] += 1
    elif answer.question_choice_answer == 22:
        question_results['answer_2_results'] += 1


def save_question_2(questions_results, writer):
    q = questions_results.data[1]['question']
    qa = questions_results.data[1]['answers']
    for i in range(1, 3):
        a = questions_results.data[1]['answer_%s_content' % i]
        ar = questions_results.data[1]['answer_%s_results' % i]
        qar = ar/float(qa)*100 if qa else 0
        writer.writerow([q, '', a, ar, "%.2f%%" % qar])


#---------------------------------------------------
#--- QUESTION 3
#---------------------------------------------------

def add_to_question_3(question_results, answer):
    """
    Dodaje odpowiedź do struktury pytania 3. Obie są przekazywane jako parametry.
    """
    question_results['answers'] += 1
    if answer.question_choice_answer == 31:
        question_results['answer_1_results'] += 1
    elif answer.question_choice_answer == 32:
        question_results['answer_2_results'] += 1
    elif answer.question_choice_answer == 33:
        question_results['answer_3_results'] += 1
    elif answer.question_choice_answer == 34:
        question_results['answer_4_results'] += 1


def save_question_3(questions_results, writer):
    q = questions_results.data[2]['question']
    qa = questions_results.data[2]['answers']
    for i in range(1, 5):
        a = questions_results.data[2]['answer_%s_content' % i]
        ar = questions_results.data[2]['answer_%s_results' % i]
        qar = ar/float(qa)*100 if qa else 0
        writer.writerow([q, '', a, ar, "%.2f%%" % qar])


#---------------------------------------------------
#--- QUESTION 4
#---------------------------------------------------

def add_to_question_4(question_results, answer):
    """
    Dodaje odpowiedź do struktury pytania 4. Obie są przekazywane jako parametry.
    """
    question_results['answers'] += 1
    if answer.question_choice_answer == 41:
        question_results['answer_1_results'] += 1
    elif answer.question_choice_answer == 42:
        question_results['answer_2_results'] += 1
    elif answer.question_choice_answer == 43:
        question_results['answer_3_results'] += 1
    elif answer.question_choice_answer == 44:
        question_results['answer_4_results'] += 1
    elif answer.question_choice_answer == 45:
        question_results['answer_5_results'] += 1


def save_question_4(questions_results, writer):
    q = questions_results.data[3]['question']
    qa = questions_results.data[3]['answers']
    for i in range(1, 6):
        a = questions_results.data[3]['answer_%s_content' % i]
        ar = questions_results.data[3]['answer_%s_results' % i]
        qar = ar/float(qa)*100 if qa else 0
        writer.writerow([q, '', a, ar, "%.2f%%" % qar])

#---------------------------------------------------
#--- QUESTION 5
#---------------------------------------------------


def add_to_question_5(question_results, answer):
    """
    Dodaje odpowiedź do struktury pytania 4. Obie są przekazywane jako parametry.
    """
    if answer.question_text_answer:
        question_results['answers'].append(answer.question_text_answer)


def save_question_5(questions_results, writer):
    q = questions_results.data[4]['question']
    answers = questions_results.data[4]['answers']
    for answer in answers:
        writer.writerow([q, '', unicode(answer).encode('utf-8'), '', ''])


def statistics_q_save(request, course_id):
    # Pobranie danych do analizy
    course = get_object_or_404(Course, id=course_id)

    if course.level.id == 8:  # Studia 1 stopnia
        answers = StudentAnswer.objects.filter(
            declaration__course_name=course.name,
            declaration__education_level=course.level,
            declaration__course_type=course.type,
        )
    else:
        answers = StudentAnswer.objects.filter(
            declaration__course_name=course.name,
            declaration__education_level=course.level,
            declaration__course_type=course.type,
            declaration__course_profile=course.profile
        )

    questions_results = QuestionsResults()
    questions_results.fill(answers)

    # Stworzenie pliku z danymi do analizy
    output = io.BytesIO()
    writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

    # Stworzenie pliku CSV
    writer.writerow(['pytanie', 'podpytanie', 'odpowiedź', 'liczba odpowiedzi', 'wynik'])

    save_question_1(questions_results, writer)
    save_question_2(questions_results, writer)
    save_question_3(questions_results, writer)
    save_question_4(questions_results, writer)
    save_question_5(questions_results, writer)

    # Stworzenie nazwy pliku do analizy
    filename = u''
    filename += utf2ascii(unicode(course))
    filename += u' - ankieta jakości kształcenia.csv'
    filename = filename.replace(',', ' ')  # Przecinki nie były akceptowan w nazwie pliku
    response = HttpResponse(output.getvalue(), mimetype='text/csv')
    response['Content-Disposition'] = 'filename="%s"' % unicode(filename).encode('utf-8')
    return response