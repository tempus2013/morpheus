# -*- coding: utf-8 -*-
'''
Created on 18-11-2014

@author: pwierzgala
'''

from django.core.management.base import BaseCommand
from apps.trainman.models import Department
from apps.merovingian.models import Course
from apps.morpheus.models import MorpheusProfile

import syjon
from django.conf import settings
from django.utils import translation


#25  # Wydział Artystyczny
#24  # Wydział Biologii i Biotechnologii
#23  # Wydział Chemii
#22  # Wydział Ekonomiczny
#8  # Wydział Humanistyczny
#2  # Wydział Matematyki, Fizyki i Informatyki
#27  # Wydział Nauk o Ziemi i Gospodarki Przestrzennej
#15  # Wydział Pedagogiki i Psychologii
#13  # Wydział Politologii
#16  # Wydział Filozofii i Socjologii
#60  # Wydział Prawa i Administracji

class Command(BaseCommand):

    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        morpheus_profile_id = args[0]
        department_id = args[1]

        morpheus_profile = MorpheusProfile.objects.get(id=morpheus_profile_id)
        department = Department.objects.get(id=department_id)
        department_children = department.children_id()
        courses = Course.objects.filter(department__id__in=department_children)

        morpheus_profile.courses.clear()
        for course in courses:
            morpheus_profile.courses.add(course)