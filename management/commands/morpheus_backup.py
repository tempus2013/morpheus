# -*- coding: utf-8 -*-
'''
Created on 18-11-2014

@author: pwierzgala
'''

from django.core.management.base import BaseCommand

import csv
import io

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

from django.db.models import Q, Sum, Count
from apps.trainman.models import Department
from apps.morpheus.models import LearningOutcomesEvaluation, StudentAnswer
from apps.merovingian.models import Course, CourseLevel
from apps.trinity.models import EducationCategory, CourseLearningOutcome

from apps.morpheus.views import get_los_evaluation_for_category, QuestionsResults, save_question_1, save_question_2, save_question_3, save_question_4, save_question_5
from apps.syjon.lib.functions import utf2ascii

import syjon
from django.conf import settings
from django.utils import translation

from apps.morpheus.config import QUESTION_1_ID, SUBQUESTION_1_ID, SUBQUESTION_2_ID, SUBQUESTION_3_ID, SUBQUESTION_4_ID, SUBQUESTION_5_ID, \
    SUBQUESTION_6_ID, SUBQUESTION_7_ID, SUBQUESTION_8_ID, SUBQUESTION_9_ID, SUBQUESTION_10_ID, QUESTION_1_ANSWER_1_ID, QUESTION_1_ANSWER_1_CONTENT, \
    QUESTION_1_ANSWER_2_ID, QUESTION_1_ANSWER_2_CONTENT, QUESTION_1_ANSWER_3_ID, QUESTION_1_ANSWER_3_CONTENT, QUESTION_1_ANSWER_4_ID, \
    QUESTION_1_ANSWER_4_CONTENT, SUBQUESTION_1_CONTENT, SUBQUESTION_2_CONTENT, SUBQUESTION_3_CONTENT, SUBQUESTION_4_CONTENT, SUBQUESTION_5_CONTENT, \
    SUBQUESTION_6_CONTENT, SUBQUESTION_7_CONTENT, SUBQUESTION_8_CONTENT, SUBQUESTION_9_CONTENT, SUBQUESTION_10_CONTENT, QUESTION_1_CONTENT
from apps.morpheus.config import QUESTION_2_ID, QUESTION_2_CONTENT, QUESTION_2_ANSWER_1_ID, QUESTION_2_ANSWER_1_CONTENT, QUESTION_2_ANSWER_2_ID, \
    QUESTION_2_ANSWER_2_CONTENT
from apps.morpheus.config import QUESTION_3_ID, QUESTION_3_CONTENT, QUESTION_3_ANSWER_1_ID, QUESTION_3_ANSWER_1_CONTENT, QUESTION_3_ANSWER_2_ID, \
    QUESTION_3_ANSWER_2_CONTENT, QUESTION_3_ANSWER_3_ID, QUESTION_3_ANSWER_3_CONTENT, QUESTION_3_ANSWER_4_ID, QUESTION_3_ANSWER_4_CONTENT
from apps.morpheus.config import QUESTION_4_ID, QUESTION_4_CONTENT, QUESTION_4_ANSWER_1_ID, QUESTION_4_ANSWER_1_CONTENT, QUESTION_4_ANSWER_2_ID, \
    QUESTION_4_ANSWER_2_CONTENT, QUESTION_4_ANSWER_3_ID, QUESTION_4_ANSWER_3_CONTENT, QUESTION_4_ANSWER_4_ID, QUESTION_4_ANSWER_4_CONTENT, \
    QUESTION_4_ANSWER_5_ID, QUESTION_4_ANSWER_5_CONTENT
from apps.morpheus.config import QUESTION_5_ID, QUESTION_5_CONTENT


class Command(BaseCommand):
    help = u'Zapisuje ankiety oceny jakości kształcenia do pliku CSV'

    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        # Pobranie wszystkich wydziałów i jednostki UMCS
        #self.save_learning_outcomes_evaluation()
        #self.save_q_questions()
        self.save_q_answers()

    def save_q_answers(self):
        # Stworzenie pliku z danymi do analizy
        output = open('ankieta oceny jakości kształcenia - odpowiedzi.csv', 'w+')
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

        # Dodanie nagłówka pliku
        header = []
        header += ['id respondenta', 'nazwa kierunku', 'stopień kierunku', 'wydział']

        # Pytanie 1
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_1_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_2_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_3_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_4_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_5_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_6_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_7_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_8_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_9_ID))
        header.append('{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_10_ID))

        # Pytanie 2
        header.append('{0}'.format(QUESTION_2_ID))

        # Pytanie 3
        header.append('{0}'.format(QUESTION_3_ID))

        # Pytanie 4
        header.append('{0}'.format(QUESTION_4_ID))

        # Pytanie 5
        header.append('{0}'.format(QUESTION_5_ID))

        writer.writerow(header)

        students = StudentAnswer.objects.all().distinct('declaration')
        for student in students:
            print student.declaration.id
            row = []
            row.append(student.declaration.id)
            row.append(unicode(student.declaration.course_name).encode('utf-8'))

            try:
                course = Course.objects.filter(name=student.declaration.course_name)[0]
                department = self.get_department(course)
            except IndexError:
                department = self.get_department(student.declaration.course_name)

            course_level = CourseLevel.objects.get(id=student.declaration.education_level_id)
            row.append(course_level)
            row.append(department)

            # Pytanie 1
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_1_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_2_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_3_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_4_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_5_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_6_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_7_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_8_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_9_ID).question_choice_answer)
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_1_ID, subquestion=SUBQUESTION_10_ID).question_choice_answer)

            # Pytanie 2
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_2_ID).question_choice_answer)

            # Pytanie 3
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_3_ID).question_choice_answer)

            # Pytanie 4
            row.append(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_4_ID).question_choice_answer)

            # Pytanie 5
            row.append(unicode(StudentAnswer.objects.get(declaration=student.declaration, question=QUESTION_5_ID).question_text_answer).encode('utf-8'))

            writer.writerow(row)

    def save_q_questions(self):

        output = open('ankieta oceny jakości kształcenia - pytania.csv', 'w+')
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

        # Pytanie 1
        answers = []
        answers.append([writer.writerow(['{0}'.format(QUESTION_1_ANSWER_1_ID), '{0}'.format(unicode(QUESTION_1_ANSWER_1_CONTENT).encode('utf-8'))])])
        answers.append([writer.writerow(['{0}'.format(QUESTION_1_ANSWER_2_ID), '{0}'.format(unicode(QUESTION_1_ANSWER_2_CONTENT).encode('utf-8'))])])
        answers.append([writer.writerow(['{0}'.format(QUESTION_1_ANSWER_3_ID), '{0}'.format(unicode(QUESTION_1_ANSWER_3_CONTENT).encode('utf-8'))])])
        answers.append([writer.writerow(['{0}'.format(QUESTION_1_ANSWER_4_ID), '{0}'.format(unicode(QUESTION_1_ANSWER_4_CONTENT).encode('utf-8'))])])
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_1_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_1_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_2_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_2_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_3_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_3_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_4_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_4_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_5_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_5_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_6_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_6_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_7_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_7_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_8_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_8_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_9_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_9_CONTENT).encode('utf-8'))])
        writer.writerows(answers)
        writer.writerow(['{0}.{1}'.format(QUESTION_1_ID, SUBQUESTION_10_ID), '{0} - {1}'.format(unicode(QUESTION_1_CONTENT).encode('utf-8'), unicode(SUBQUESTION_10_CONTENT).encode('utf-8'))])
        writer.writerows(answers)

        # Pytanie 2
        writer.writerow(['{0}'.format(QUESTION_2_ID), u'{0}'.format(unicode(QUESTION_2_CONTENT)).encode('utf-8')])
        writer.writerow(['{0}'.format(QUESTION_2_ANSWER_1_ID), '{0}'.format(unicode(QUESTION_2_ANSWER_1_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_2_ANSWER_2_ID), '{0}'.format(unicode(QUESTION_2_ANSWER_2_CONTENT).encode('utf-8'))])

        # Pytanie 3
        writer.writerow(['{0}'.format(QUESTION_3_ID), '{0}'.format(unicode(QUESTION_3_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_3_ANSWER_1_ID), '{0}'.format(unicode(QUESTION_3_ANSWER_1_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_3_ANSWER_2_ID), '{0}'.format(unicode(QUESTION_3_ANSWER_2_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_3_ANSWER_3_ID), '{0}'.format(unicode(QUESTION_3_ANSWER_3_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_3_ANSWER_4_ID), '{0}'.format(unicode(QUESTION_3_ANSWER_4_CONTENT).encode('utf-8'))])

        # Pytanie 4
        writer.writerow(['{0}'.format(QUESTION_4_ID), '{0}'.format(unicode(QUESTION_4_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_4_ANSWER_1_ID), '{0}'.format(unicode(QUESTION_4_ANSWER_1_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_4_ANSWER_2_ID), '{0}'.format(unicode(QUESTION_4_ANSWER_2_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_4_ANSWER_3_ID), '{0}'.format(unicode(QUESTION_4_ANSWER_3_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_4_ANSWER_4_ID), '{0}'.format(unicode(QUESTION_4_ANSWER_4_CONTENT).encode('utf-8'))])
        writer.writerow(['{0}'.format(QUESTION_4_ANSWER_5_ID), '{0}'.format(unicode(QUESTION_4_ANSWER_5_CONTENT).encode('utf-8'))])

        # Pytanie 5
        writer.writerow(['{0}'.format(QUESTION_5_ID), '{0}'.format(unicode(QUESTION_5_CONTENT).encode('utf-8'))])

    def save_learning_outcomes_evaluation(self):
        # Stworzenie pliku z danymi do analizy
        output = open('ankieta_oceny_efektów_kształcenia.csv', 'w+')
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

        # Dodanie nagłówka pliku
        writer.writerow(['id respondenta', 'nazwa kierunku', 'stopień kierunku', 'profil kierunku', 'symbol efektu', 'treść efektu', 'kategoria efektu', 'ocena efektu (0-2)'])

        learning_outcomes_evaluations = LearningOutcomesEvaluation.objects.all().order_by('graduate_declaration__id')
        for learning_outcomes_evaluation in learning_outcomes_evaluations:
            print learning_outcomes_evaluation.id
            row = []
            row.append(learning_outcomes_evaluation.graduate_declaration.id)
            row.append(unicode(learning_outcomes_evaluation.course.name).encode('utf-8'))
            row.append(unicode(learning_outcomes_evaluation.course.level).encode('utf-8'))
            row.append(unicode(learning_outcomes_evaluation.course.profile).encode('utf-8'))
            row.append(learning_outcomes_evaluation.course_learning_outcome.symbol)
            row.append(unicode(learning_outcomes_evaluation.course_learning_outcome.description).encode('utf-8'))
            row.append(learning_outcomes_evaluation.course_learning_outcome.education_category)
            row.append(learning_outcomes_evaluation.evaluation)
            writer.writerow(row)

    def get_department(self, course):
        """
        Zwraca wydział kierunku lub None jeżeli nie udało się ustalić wydziału.
        """
        print course, u'Matematyka', course == u'Matematyka'
        if type(course) == unicode:
            if course == u'Matematyka' or course == u'Matematyka z informatyką':
                return Department.objects.get(id=2)
            elif course == u'Edukacja artystyczna w zakresie sztuk plasycznych':
                return Department.objects.get(id=25)
            elif course == u'Filologia romańska':
                return Department.objects.get(id=8)
            return '-'
        else:
            department = course.department
            if department:
                if department.type and department.type.id == 1:
                    return department

                while department.id != 1:
                    if department.type and department.type.id == 1:
                        return department
                    department = department.department
                    if not department:
                        return None
            return '-'



