# -*- coding: utf-8 -*-
'''
Created on 18-11-2014

@author: pwierzgala
'''

from django.core.management.base import BaseCommand

import csv
import io

from django.utils import termcolors
green = termcolors.make_style(fg='green')
yellow = termcolors.make_style(fg='yellow')
cyan = termcolors.make_style(fg='cyan')
red = termcolors.make_style(fg='red', opts=('bold',))
bold = termcolors.make_style(opts=('bold',))

from django.db.models import Q, Sum, Count
from apps.trainman.models import Department
from apps.morpheus.models import LearningOutcomesEvaluation, StudentAnswer
from apps.merovingian.models import Course
from apps.trinity.models import EducationCategory, CourseLearningOutcome

from apps.morpheus.views import get_los_evaluation_for_category, QuestionsResults, save_question_1, save_question_2, save_question_3, save_question_4, save_question_5
from apps.syjon.lib.functions import utf2ascii

import syjon
from django.conf import settings
from django.utils import translation


class Command(BaseCommand):
    help = u'Dodaje sylabusy modułu z pliku.'

    def handle(self, *args, **options):
        translation.activate(getattr(settings, 'LANGUAGE_CODE', syjon.settings.LANGUAGE_CODE))

        # Pobranie wszystkich wydziałów i jednostki UMCS
        departments = Department.objects.filter(Q(type_id=1) | Q(id=1))
        self.create_q_results(departments)
        self.create_los_results(departments)

    def create_q_results(self, departments):
        for department in departments:
            # Wyniki dla studiów I stopnia
            results = self.get_q_results_for_level(department, 8)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'I stopnia', u'ankieta jakości kształcenia')  # Stworzenie nazwy pliku (przecinki nie były akceptowane w nazwie pliku)
            self.save_q_results(results, filename)

            # Wyniki dla studiów II stopnia
            results = self.get_q_results_for_level(department, 9)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'II stopnia', u'ankieta jakości kształcenia')  # Stworzenie nazwy pliku (przecinki nie były akceptowane w nazwie pliku)
            self.save_q_results(results, filename)

            # Wyniki dla studentów studiów jednolitych magisterskich
            results = self.get_q_results_for_level(department, 11)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'jednolite magisterskie', u'ankieta jakości kształcenia')  # Stworzenie nazwy pliku (przecinki nie były akceptowane w nazwie pliku)
            self.save_q_results(results, filename)

    def get_q_results_for_level(self, department, level):
        # Stworzenie listy kierunków w jednostce
        department_courses = Course.objects.filter(department__in=department.children(), level__id=level)
        department_courses = department_courses.values('name', 'level', 'type', 'profile').order_by('name', 'level', 'type', 'profile').distinct()
        questions_results = QuestionsResults()
        for course in department_courses:
            # Pobranie rezultatów dla kierunku
            answers = StudentAnswer.objects.filter(
                declaration__course_name=course['name'],
                declaration__education_level=course['level'],
                declaration__course_type=course['type'],
                declaration__course_profile=course['profile']
            )
            # Dodanie wyników do struktury z wynikami
            if answers.count() > 0:
                questions_results.fill(answers)
        return questions_results

    def save_q_results(self, results, filename):
        # Stworzenie pliku z danymi do analizy
        output = open(filename, 'w+')
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

        # Dodanie nagłówka pliku
        writer.writerow(['pytanie', 'podpytanie', 'odpowiedź', 'liczba odpowiedzi', 'wynik'])

        # Zapisanie wyników do pliku
        save_question_1(results, writer)
        save_question_2(results, writer)
        save_question_3(results, writer)
        save_question_4(results, writer)
        # save_question_5(questions_results, writer)

    def create_los_results(self, departments):
        # Pobranie obiektów kategorii kształcenia
        knowledge = EducationCategory.objects.get(name_pl='wiedza')
        skills = EducationCategory.objects.get(name_pl='umiejętności')
        competences = EducationCategory.objects.get(name_pl='kompetencje społeczne')
        categories = (knowledge, skills, competences)

        # Wygenerowanie wyników dla wszystkich wydziałów
        for department in departments:
            # Wyniki dla studentów I stopnia
            results = self.get_los_results_for_level(department, 8, categories)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'I stopnia', u'ocena efektów kształcenia')
            self.save_los_results(results, categories, filename)

            # Wyniki dla studentów II stopnia
            results = self.get_los_results_for_level(department, 9, categories)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'II stopnia', u'ocena efektów kształcenia')
            self.save_los_results(results, categories, filename)

            # Wyniki dla studentów studiów jednolitych magisterskich
            results = self.get_los_results_for_level(department, 11, categories)
            filename = u"%s - %s - %s.csv".replace(',', ' ') % (utf2ascii(unicode(department)), u'jednolite magisterskie', u'ocena efektów kształcenia')
            self.save_los_results(results, categories, filename)

    def get_los_results_for_level(self, department, level, categories):
        knowledge = categories[0]
        skills = categories[1]
        competences = categories[2]

        # Stworzenie listy na wyniki
        x = {knowledge: None, skills: None, competences: None}
        for category in categories:
            x[category] = {'count': 0, 'students': 0, 'sum': 0, 1: 0, 2: 0, 3: 0}

        # Stworzenie listy kierunków w jednostce
        department_courses = Course.objects.filter(department__in=department.children(), level__id=level)
        department_courses = department_courses.values('name', 'level', 'type', 'profile').order_by('name', 'level', 'type', 'profile').distinct()

        # Zsumowanie wyników dla wszystkich kierunków w jednostce
        for cd in department_courses:

            course = Course.objects.filter(name=cd['name'], level__id=cd['level'], type__id=cd['type'], profile=cd['profile'])[0]

            # print "\t%s" % yellow(unicode(course).encode('utf-8'))
            results_knowledge = get_los_evaluation_for_category(course, knowledge)
            results_skills = get_los_evaluation_for_category(course, skills)
            results_competences = get_los_evaluation_for_category(course, competences)
            results = (results_knowledge, results_skills, results_competences)

            for c, rs in zip(categories, results):
                # Obliczenie liczby studentów, którzy ocenili kierunek
                x[c]['students'] += LearningOutcomesEvaluation.objects.filter(course__name=course.name, course__level=course.level, course__type=course.type, course__profile=course.profile).distinct('graduate_declaration').count()
                for r in rs:
                    x[c]['count'] += r['count']  # Liczba ocenionych efektów kształcenia
                    x[c]['sum'] += r['sum']  # Suma ocen efektów kształcenia
                    x[c][1] += r[1]  # Suma ocen 1
                    x[c][2] += r[2]  # Suma ocen 2
                    x[c][3] += r[3]  # Suma ocen 3
        return x

    def save_los_results(self, results, categories, filename):
        # Stworzenie pliku z danymi do analizy
        output = open(filename, 'w+')
        writer = csv.writer(output, delimiter='\t', quotechar='"', quoting=csv.QUOTE_ALL)

        # Dodanie nagłówka pliku
        writer.writerow(['kategoria', 'liczba ocen', 'średnia arytmetyczna ocen w skali 1-3', 'słabo (1)', 'przeciętnie (2)', 'dobrze (3)'])

        # Zapisanie wyników do pliku
        for c in categories:
            category = unicode(c.name_pl).encode('utf-8')
            students = results[c]['students']
            count = float(results[c]['count'])
            avg = "%.2f" % (results[c]['sum']/count) if count else 0
            a1 = "%.2f%%" % (results[c][1]/count*100) if count else 0
            a2 = "%.2f%%" % (results[c][2]/count*100) if count else 0
            a3 = "%.2f%%" % (results[c][3]/count*100) if count else 0
            writer.writerow([category, students, avg, a1, a2, a3])

