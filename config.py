# -*- coding: utf-8 -*-

#---------------------------------------------------
#--- QUESTION 1
#---------------------------------------------------

QUESTION_1_ID = 1
QUESTION_1_CONTENT = u"Oceń konstrukcję programów studiów i sposób realizacji treści"

QUESTION_1_ANSWER_1_ID = 11
QUESTION_1_ANSWER_1_CONTENT = u"Za duża ilość"

QUESTION_1_ANSWER_2_ID = 12
QUESTION_1_ANSWER_2_CONTENT = u"Odpowiednia ilość"

QUESTION_1_ANSWER_3_ID = 13
QUESTION_1_ANSWER_3_CONTENT = u"Za mała ilość"

QUESTION_1_ANSWER_4_ID = 14
QUESTION_1_ANSWER_4_CONTENT = u"Nie mam zdania"

QUESTION_1_ANSWERS = (
    (QUESTION_1_ANSWER_1_ID, QUESTION_1_ANSWER_1_CONTENT),
    (QUESTION_1_ANSWER_2_ID, QUESTION_1_ANSWER_2_CONTENT),
    (QUESTION_1_ANSWER_3_ID, QUESTION_1_ANSWER_3_CONTENT),
    (QUESTION_1_ANSWER_4_ID, QUESTION_1_ANSWER_4_CONTENT)
)

SUBQUESTION_1_ID = 1
SUBQUESTION_1_CONTENT = u"Zajęcia do wyboru"

SUBQUESTION_2_ID = 2
SUBQUESTION_2_CONTENT = u"Zajęcia specjalnościowe/specjalizacyjne"

SUBQUESTION_3_ID = 3
SUBQUESTION_3_CONTENT = u"Lektoraty"

SUBQUESTION_4_ID = 4
SUBQUESTION_4_CONTENT = u"Zajęcia wykładowe"

SUBQUESTION_5_ID = 5
SUBQUESTION_5_CONTENT = u"Zajęcia ćwiczeniowe o charakterze dyskusyjnym"

SUBQUESTION_6_ID = 6
SUBQUESTION_6_CONTENT = u"Zajęcia ćwiczeniowe kształtujące umiejętności praktyczne"

SUBQUESTION_7_ID = 7
SUBQUESTION_7_CONTENT = u"Zajęcia wymagające samodzielnej pracy studenta"

SUBQUESTION_8_ID = 8
SUBQUESTION_8_CONTENT = u"Zajęcia z zastosowaniem specjalistycznych narzędzi (np. aparatury, programów komputerowych)"

SUBQUESTION_9_ID = 9
SUBQUESTION_9_CONTENT = u"Zajęcia wymagające znajomości literatury przedmiotu"

SUBQUESTION_10_ID = 10
SUBQUESTION_10_CONTENT = u"Praktyki zawodowe"

#---------------------------------------------------
#--- QUESTION 2
#---------------------------------------------------

QUESTION_2_ID = 2
QUESTION_2_CONTENT = u"Czy studia spełniły twoje oczekiwania?"

QUESTION_2_ANSWER_1_ID = 21
QUESTION_2_ANSWER_1_CONTENT = u"Tak"

QUESTION_2_ANSWER_2_ID = 22
QUESTION_2_ANSWER_2_CONTENT = u"Nie"

QUESTION_2_ANSWERS = (
    (QUESTION_2_ANSWER_1_ID, QUESTION_2_ANSWER_1_CONTENT),
    (QUESTION_2_ANSWER_2_ID, QUESTION_2_ANSWER_2_CONTENT)
)

#---------------------------------------------------
#--- QUESTION 3
#---------------------------------------------------

QUESTION_3_ID = 3
QUESTION_3_CONTENT = u"Czy gdybyś jeszcze raz miał rozpocząć studia, wybrałbyś UMCS i ten sam kierunek?"

QUESTION_3_ANSWER_1_ID = 31
QUESTION_3_ANSWER_1_CONTENT = u"Wybrałabym UMCS i ten sam kierunek"

QUESTION_3_ANSWER_2_ID = 32
QUESTION_3_ANSWER_2_CONTENT = u"Wybrałabym UMCS ale inny kierunek"

QUESTION_3_ANSWER_3_ID = 33
QUESTION_3_ANSWER_3_CONTENT = u"Wybrałabym ten sam kierunek ale inną uczelnię"

QUESTION_3_ANSWER_4_ID = 34
QUESTION_3_ANSWER_4_CONTENT = u"Wybrałabym inną uczelnię i inny kierunek"

QUESTION_3_ANSWERS = (
    (QUESTION_3_ANSWER_1_ID, QUESTION_3_ANSWER_1_CONTENT),
    (QUESTION_3_ANSWER_2_ID, QUESTION_3_ANSWER_2_CONTENT),
    (QUESTION_3_ANSWER_3_ID, QUESTION_3_ANSWER_3_CONTENT),
    (QUESTION_3_ANSWER_4_ID, QUESTION_3_ANSWER_4_CONTENT),
)

#---------------------------------------------------
#--- QUESTION 4
#---------------------------------------------------

QUESTION_4_ID = 4
QUESTION_4_CONTENT = u"Czy zamierzasz kontynuować studia w UMCS na tym samym kierunku?"

QUESTION_4_ANSWER_1_ID = 41
QUESTION_4_ANSWER_1_CONTENT = u"Zamierzam kontynuować studia w UMCS na tym samym kierunku"

QUESTION_4_ANSWER_2_ID = 42
QUESTION_4_ANSWER_2_CONTENT = u"Zamierzam kontynuować studia w UMCS, ale wybiorę inny kierunek"

QUESTION_4_ANSWER_3_ID = 43
QUESTION_4_ANSWER_3_CONTENT = u"Zamierzam kontynuować studia w innej uczelni i wybiorę ten sam kierunek"

QUESTION_4_ANSWER_4_ID = 44
QUESTION_4_ANSWER_4_CONTENT = u"Zamierzam kontynuować studia w innej uczelni ale wybiorę inny kierunek"

QUESTION_4_ANSWER_5_ID = 45
QUESTION_4_ANSWER_5_CONTENT = u"Nie zamierzam kontynuować studiów"

QUESTION_4_ANSWERS = (
    (QUESTION_4_ANSWER_1_ID, QUESTION_4_ANSWER_1_CONTENT),
    (QUESTION_4_ANSWER_2_ID, QUESTION_4_ANSWER_2_CONTENT),
    (QUESTION_4_ANSWER_3_ID, QUESTION_4_ANSWER_3_CONTENT),
    (QUESTION_4_ANSWER_4_ID, QUESTION_4_ANSWER_4_CONTENT),
    (QUESTION_4_ANSWER_5_ID, QUESTION_4_ANSWER_5_CONTENT)
)

#---------------------------------------------------
#--- QUESTION 5
#---------------------------------------------------

QUESTION_5_ID = 5
QUESTION_5_CONTENT = u"Dodatkowe uwagi"