# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'GraduateDeclaration'
        db.create_table('morpheus_graduate_declaration', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('course_name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('education_level', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['merovingian.CourseLevel'])),
            ('album_number', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('first_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('last_name', self.gf('django.db.models.fields.CharField')(max_length=32)),
            ('approximated_defense_date', self.gf('django.db.models.fields.DateField')()),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
        ))
        db.send_create_signal('morpheus', ['GraduateDeclaration'])


    def backwards(self, orm):
        # Deleting model 'GraduateDeclaration'
        db.delete_table('morpheus_graduate_declaration')


    models = {
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'morpheus.graduatedeclaration': {
            'Meta': {'object_name': 'GraduateDeclaration', 'db_table': "'morpheus_graduate_declaration'"},
            'album_number': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'approximated_defense_date': ('django.db.models.fields.DateField', [], {}),
            'course_name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'education_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '32'})
        }
    }

    complete_apps = ['morpheus']