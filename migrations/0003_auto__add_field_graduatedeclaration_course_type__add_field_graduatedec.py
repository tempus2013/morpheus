# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'GraduateDeclaration.course_type'
        db.add_column('morpheus_graduate_declaration', 'course_type',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=6, to=orm['merovingian.CourseType']),
                      keep_default=False)

        # Adding field 'GraduateDeclaration.course_profile'
        db.add_column('morpheus_graduate_declaration', 'course_profile',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=4, to=orm['merovingian.CourseProfile']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'GraduateDeclaration.course_type'
        db.delete_column('morpheus_graduate_declaration', 'course_type_id')

        # Deleting field 'GraduateDeclaration.course_profile'
        db.delete_column('morpheus_graduate_declaration', 'course_profile_id')


    models = {
        'merovingian.courselevel': {
            'Meta': {'object_name': 'CourseLevel', 'db_table': "'merv_course_level'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.courseprofile': {
            'Meta': {'object_name': 'CourseProfile', 'db_table': "'merv_course_profile'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'merovingian.coursetype': {
            'Meta': {'object_name': 'CourseType', 'db_table': "'merv_course_type'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '256'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_pl': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ru': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'name_ua': ('django.db.models.fields.CharField', [], {'max_length': '256', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'morpheus.graduatedeclaration': {
            'Meta': {'object_name': 'GraduateDeclaration', 'db_table': "'morpheus_graduate_declaration'"},
            'album_number': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'approximated_defense_date': ('django.db.models.fields.DateField', [], {}),
            'course_name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'course_profile': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseProfile']"}),
            'course_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseType']"}),
            'education_level': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['merovingian.CourseLevel']"}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '32'}),
            'questionnaire': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        }
    }

    complete_apps = ['morpheus']