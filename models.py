# -*- coding: utf-8 -*-

from django.db import models

from django.utils.translation import ugettext_lazy as _

from apps.merovingian.models import Course, CourseLevel, CourseType, CourseProfile
from apps.trinity.models import CourseLearningOutcome
from apps.trainman.models import UserProfile


#---------------------------------------------------
#--- DEKLARACJA ABSOLWENTA
#---------------------------------------------------

class GraduateDeclaration(models.Model):
    class Meta:
        db_table = 'morpheus_graduate_declaration'
        verbose_name = _(u'Graduate declaration')
        verbose_name_plural = _(u'Graduates declarations')
    
    course_name = models.CharField(max_length=256, verbose_name=_('Course'))
    education_level = models.ForeignKey(CourseLevel, verbose_name=_('Course level'))
    course_type = models.ForeignKey(CourseType, verbose_name=_('Course type'))
    course_profile = models.ForeignKey(CourseProfile, verbose_name=_('Course profile'))
    album_number = models.CharField(verbose_name=_(u'Album number'), max_length=32)
    first_name = models.CharField(verbose_name=_(u"First name"), max_length=32)
    last_name = models.CharField(verbose_name=_('Last name'), max_length=32)
    approximated_defense_date = models.DateField(verbose_name=_('Approximated defense date'))
    email = models.EmailField(verbose_name=_(u'Email'))
    questionnaire = models.BooleanField(verbose_name=_('Accepted participation in questionnaire'))
    creation_date = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return "%s %s" % (self.first_name, self.last_name)


#---------------------------------------------------
#--- OCENA EFEKTÓW KSZTAŁCENIA
#---------------------------------------------------

class LearningOutcomesEvaluation(models.Model):
    class Meta:
        db_table = 'morpheus_learning_outcomes_evaluation'
        verbose_name = _(u'Learning outcomes evaluation')
        verbose_name_plural = _(u'Learning outcomes evaluations')
        ordering = ('course_learning_outcome__symbol', )

    graduate_declaration = models.ForeignKey(GraduateDeclaration, verbose_name=_('GraduateDeclaration'))
    course = models.ForeignKey(Course, verbose_name=_('Course'))
    course_learning_outcome = models.ForeignKey(CourseLearningOutcome, verbose_name=_('Course learning outcome'))
    evaluation = models.IntegerField(verbose_name=_('Evaluation'))


#---------------------------------------------------
#--- ANKIETA
#---------------------------------------------------

class StudentAnswer(models.Model):
    declaration = models.ForeignKey(GraduateDeclaration)
    question = models.IntegerField()
    subquestion = models.IntegerField(null=True, blank=True)
    question_choice_answer = models.IntegerField(null=True, blank=True)
    question_text_answer = models.TextField(null=True, blank=True)


#---------------------------------------------------
#--- PROFIL
#---------------------------------------------------

from django.db.models import Max
from apps.morpheus.managers import MorpheusProfileManager


class MorpheusProfile(models.Model):
    """
    Administrator statystyk jakości kształcenia.
    """
    class Meta:
        db_table = 'morpheus_profile'
        verbose_name = _(u'Administrator')
        verbose_name_plural = _(u'Administrators')

    user_profile = models.OneToOneField(UserProfile, verbose_name=_(u'User'))
    courses = models.ManyToManyField(Course, db_table='morpheus_profile__to__course', null=True, blank=True)

    objects = MorpheusProfileManager()

    def __unicode__(self):
        return self.user_profile.user.username