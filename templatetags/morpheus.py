# -*- coding: utf-8 -*-

from django import template
from apps.trinity.models import ModuleLearningOutcome

register = template.Library()


@register.inclusion_tag('morpheus/templatetags/los_results.html')
def los_table(header, results):
    return {'header': header, 'results': results}


@register.simple_tag
def big_count(results):
    value = 0
    for result in results:
        value += result['count']
    return value


@register.simple_tag
def big_sum(results):
    value = 0
    for result in results:
        value += result['sum']
    return value


@register.simple_tag
def big_avg(results):
    count = 0.0
    summ = 0.0
    for result in results:
        count += result['count']
        summ += result['sum']

    return "%.2f%%" % ((summ/count)/2.0*100)


@register.simple_tag
def number_to_percent(number, numbers):
    return "%.2f%%" % (number/float(numbers)*100)