# -*- coding: UTF-8 -*-
from django.db import models
from django.db.models import Max
from django.core.exceptions import ObjectDoesNotExist

from apps.merovingian.models import Course


class MorpheusProfileManager(models.Manager):

    def get_courses_names(self, user):
        if user.is_superuser:
            return Course.objects.values('name').distinct().annotate(id=Max('id')).order_by('name')
        else:
            try:
                return self.get(user_profile=user.get_profile()).courses.values('name').distinct().annotate(id=Max('id')).order_by('name')
            except ObjectDoesNotExist:
                return []