# -*- coding: utf-8 -*-

from django import forms
from django.forms.extras.widgets import SelectDateWidget
from django.utils.translation import ugettext_lazy as _

from datetime import date

from apps.merovingian.models import Course, CourseLevel, CourseType, CourseProfile

#---------------------------------------------------
#--- DEKLARACJA ABSOLWENTA
#---------------------------------------------------

DEFENSE_DATE_YEARS = [n for n in range(date.today().year-1, date.today().year+3)]
SELECT_RESPONDENTS_YEARS = [n for n in range(date.today().year-20, date.today().year)]


class GraduateDeclarationForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=_(u'First name'))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=_(u'Last name'))
    album_number = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=_(u'Album number'))
    course = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=Course.objects.values_list('name', 'name').distinct().order_by('name'), label=_(u'Course'))
    education_level = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=CourseLevel.objects.all(), label=_(u'Course level'))
    course_type = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=CourseType.objects.all(), label=_(u'Major type'))
    course_profile = forms.ModelChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), queryset=CourseProfile.objects.all(), label=_(u'Course profile'))
    approximated_defense_date = forms.DateField(widget=SelectDateWidget(years=DEFENSE_DATE_YEARS, attrs={'class': 'form-control', 'style': 'display: inline; width: auto;'}), label=_(u'Approximated defense date'))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class': 'form-control'}), label=_(u'Email'))
    questionaire = forms.ChoiceField(widget=forms.Select(attrs={'class': 'form-control'}), choices=(('0', _(u'No')), ('1', _(u'Yes'))), initial=1, label=_(u"I agree to take part in a graduates' questionaire"), required=False)


#---------------------------------------------------
#--- OCENA EFEKTÓW KSZTAŁCENIA
#---------------------------------------------------

class LearningOutcomesEvaluationForm(forms.Form):
    def __init__(self, *args, **kwargs):
        learning_outcomes = kwargs.pop('learning_outcomes', None)
        super(LearningOutcomesEvaluationForm, self).__init__(*args, **kwargs)
        for learning_outcome in learning_outcomes:
            self.fields['%s' % learning_outcome.id] = forms.ChoiceField(choices=(('0', 'słabo'), ('1', 'przeciętnie'), ('2', 'dobrze')), widget=forms.RadioSelect(), label=learning_outcome.description)


class SelectRespondentsForm(forms.Form):
    start_date = forms.DateField(widget=SelectDateWidget(years=SELECT_RESPONDENTS_YEARS), label=_(u'Start date'))
    end_date = forms.DateField(widget=SelectDateWidget(years=SELECT_RESPONDENTS_YEARS), label=_(u'End date'))
    education_level = forms.ModelChoiceField(queryset=CourseLevel.objects.all(), label=_(u'Education level'))


#---------------------------------------------------
#--- ANKIETA
#---------------------------------------------------

from apps.morpheus.config import *


class Question1Form(forms.Form):
    question = QUESTION_1_CONTENT
    answers = QUESTION_1_ANSWERS
    subquestion_1 = forms.ChoiceField(label=SUBQUESTION_1_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_2 = forms.ChoiceField(label=SUBQUESTION_2_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_3 = forms.ChoiceField(label=SUBQUESTION_3_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_4 = forms.ChoiceField(label=SUBQUESTION_4_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_5 = forms.ChoiceField(label=SUBQUESTION_5_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_6 = forms.ChoiceField(label=SUBQUESTION_6_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_7 = forms.ChoiceField(label=SUBQUESTION_7_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_8 = forms.ChoiceField(label=SUBQUESTION_8_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_9 = forms.ChoiceField(label=SUBQUESTION_9_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())
    subquestion_10 = forms.ChoiceField(label=SUBQUESTION_10_CONTENT, choices=QUESTION_1_ANSWERS, widget=forms.RadioSelect())


class Question2Form(forms.Form):
    question = QUESTION_2_CONTENT
    answers = forms.ChoiceField(choices=QUESTION_2_ANSWERS, widget=forms.RadioSelect())


class Question3Form(forms.Form):
    question = QUESTION_3_CONTENT
    answers = forms.ChoiceField(choices=QUESTION_3_ANSWERS, widget=forms.RadioSelect())


class Question4Form(forms.Form):
    question = QUESTION_4_CONTENT
    answers = forms.ChoiceField(choices=QUESTION_4_ANSWERS, widget=forms.RadioSelect())


class Question5Form(forms.Form):
    question = QUESTION_5_CONTENT
    answer = forms.CharField(widget=forms.Textarea(), required=False)